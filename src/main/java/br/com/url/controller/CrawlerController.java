package br.com.url.controller;

import br.com.url.model.CrawlerModel;

// import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class CrawlerController {
  
  private final CrawlerModel model = new CrawlerModel();
  
  // @RequestMapping(value = "/url/{address}", method = RequestMethod.GET)
  // public String getURLs(@PathVariable(value="address") String address) {
  @RequestMapping(value = "/url/", method = RequestMethod.GET)
  public String getURLs(@RequestParam("address") String address) {
    
    return this.model.findLinks(address);
  }  
  
}