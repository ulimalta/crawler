package br.com.url.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.cj.jdbc.MysqlDataSource;

public class DBManager {
  
  private MysqlDataSource dataSource;
  
  public DBManager() {
    this.dataSource = new MysqlDataSource();
    this.dataSource.setUser("root");
    this.dataSource.setPassword("root");
    this.dataSource.setServerName("localhost");
  }
  
  public ResultSet select(String query) {
    ResultSet resp = null;
    Connection conn;
    try {
      conn = dataSource.getConnection();
      Statement stmt = conn.createStatement();
      resp = stmt.executeQuery(query);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return resp;
  }

}
