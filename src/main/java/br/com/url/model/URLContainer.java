package br.com.url.model;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.commons.validator.routines.UrlValidator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class URLContainer {

  private String parentURL;
  private ArrayList<String> childs;
  private UrlValidator urlValidator;

  public URLContainer(String parentURL) {
    this.parentURL = parentURL;
    this.childs = new ArrayList<String>();
    this.urlValidator = new UrlValidator(UrlValidator.ALLOW_ALL_SCHEMES +
        UrlValidator.ALLOW_LOCAL_URLS);
  }

  public String getParentURL() {
    return this.parentURL;
  }

  public ArrayList<String> getChilds() {
    return this.childs;
  }

  public boolean isURLValid(String url) {
    return this.urlValidator.isValid(url);
  }

  public boolean addURL(String url) {
    // The URL must be valid and repeated URLs should not be considered
    if (this.isURLValid(url) && !this.contains(url)) {
      this.childs.add(url);
      return true;
    }
    return false;
  }

  public boolean contains(String url) {
    return this.childs.contains(url);
  }

  private void explore(String url) {
    System.out.println(url);
    if (!this.addURL(url)) {
      return;
    }
    try {
      Document doc = Jsoup.connect(url).get();
      Elements elements = doc.select("a");
      for (Element element : elements) {
        this.explore(element.absUrl("href"));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public ArrayList<String> getLinks() {
    this.explore(this.parentURL);
    return this.childs;
  }

}
