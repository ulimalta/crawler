package br.com.url.model;

import java.util.ArrayList;

public class CrawlerModel {

  public String findLinks(String url) {
    URLContainer cont = new URLContainer(url);
    ArrayList<String> resp = cont.getLinks();
    return resp.toString();
  }
  
}
