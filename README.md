Crawler que recupera recursivamente os links acessíveis a partir de uma página da internet.

O programa pode ser ccompilado com o maven e, depois, ser executado em um servidor tomcat
(os testes estão sendo feitos no tomcat 9.0.17).

Nesse setup local, basta acessar a URL da aplicação e passar como parâmetro a URL base de
interesse. Exemplo:

http://localhost:8080/crawler/url/?address=http://www.students.ic.unicamp.br/~ra140958

Neste caso, a URL base seria: http://www.students.ic.unicamp.br/~ra140958

O projeto ainda está em desenvolvimento. Pontos a serem traatados ainda:

- Adição de testes

- Término da implementação da funcionalidade para salvamento e recuperação das
informações em banco de dados

- Deploy na cloud

- Melhoria de desempenho. As páginas atualmente possuem muitos links, o que pode
fazer com que a busca recursiva demore muito. Talvez implementar alguma política
de paginação do resultado?
